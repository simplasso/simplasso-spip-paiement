<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


function simplpaie_post_insertion($flux)
{

    include_spip('simplasso_fonctions');
    if ($flux['args']['table'] === 'spip_transactions') {
        $transaction = sql_fetsel("*", "spip_transactions", "id_transaction=" . ((int)$flux['args']['id_objet']));
        $data = [
            'transaction' => json_encode($transaction),
        ];
        include_spip('inc/session');
        $token = session_get('simplasso_user_token');
        if (empty($token)) {
            $result = interrogeAPITransaction('transaction/inscription', $data);
        } else {
            $result = interrogeAPI('transaction_individu/ajouter', $data);
        }

        if ($result) {

            spip_log('Erreur dans la duplication dans simplasso de la transaction', _LOG_HS);
        }
    }
    return $flux;
}


function simplpaie_bank_traiter_reglement($flux)
{
    include_spip('simplasso_fonctions');
    /* 'args'=>array(
         'id_transaction'=>$id_transaction,
         'new'=>$row_prec['reglee']!=='oui',
         'confirm'=>$row_prec['reglee']=='oui',
         'notifier'=>$notifier,
         'avant'=>$row_prec,
         'options' => $options,
     ),
         'data'=>$message
    */
    /*
    if ($flux['args']['confirm']) {


    }*/
    $transaction = sql_fetsel("*", "spip_transactions", "id_transaction=" . ((int)$flux['args']['id_transaction']));


    $data = [
        'transaction' => json_encode($transaction)
    ];

    if ($transaction['statut'] === 'ok' && $transaction['reglee'] === 'oui') {
        $result = interrogeAPITransaction('transaction/valider', $data);
        if (!isset($result['ok'])) {
            exit();
        }
    } else {
        $result = interrogeAPITransaction('transaction/echec', $data);
    }

    return $flux;
}


function simplpaie_trig_bank_reglement_en_attente($flux)
{

    include_spip('simplasso_fonctions');
    $transaction = sql_fetsel("*", "spip_transactions", "id_transaction=" . ((int)$flux['args']['id_transaction']));
    $data = [
        'transaction' => json_encode($transaction),
    ];
    include_spip('simplasso_fonctions');
    if ($transaction['statut'] === 'attente') {
        $result = interrogeAPITransaction('transaction/attente', $data);
    }
    return $flux;


}


function simplpaie_bank_dsp2_renseigner_facturation($flux)
{
    include_spip('simplasso_fonctions');
    $tab_info = [];

    $ok = false;
    if (session_get('simplasso_user_token')) {

        $info_adh = interrogeAPI('info_adherent');
        if (isset($info_adh['email'])) {
            $tab_info = [
                'nom' => $info_adh['nom'],
                'prenom' => $info_adh['prenom'],
                'email' => $info_adh['email'],
                'adresse' => $info_adh['adresse'],
                'code_postal' => $info_adh['codepostal'],
                'ville' => $info_adh['ville'],
                'etat' => '',
                'pays' => $info_adh['pays']
            ];
        }

    } elseif (session_get('inscription_adh1')) {
        include_spip('inc/session');
        $info_adh = session_get('inscription_adh1');
        if (isset($info_adh['email'])) {
            $tab_info = [
                'nom' => $info_adh['nom_famille'],
                'prenom' => $info_adh['prenom'],
                'email' => $info_adh['email'],
                'adresse' => $info_adh['adresse'],
                'code_postal' => $info_adh['code_postal'],
                'ville' => $info_adh['ville'],
                'etat' => '',
                'pays' => $info_adh['pays']
            ];
            $ok = true;
        }
    }

    if (!$ok) {
        $info_adh = json_decode($flux['args']['contenu'], true);
        $info_adh = $info_adh["individu"] ?? [];
        if (isset($info_adh['email'])) {
            $tab_info = [
                'nom' => $info_adh['nom_famille'],
                'prenom' => $info_adh['prenom'],
                'email' => $info_adh['email'],
                'adresse' => $info_adh['adresse'],
                'code_postal' => $info_adh['code_postal'],
                'ville' => $info_adh['ville'],
                'etat' => '',
                'pays' => $info_adh['pays']
            ];
            $ok = true;
        }
    }


    return $tab_info;
}

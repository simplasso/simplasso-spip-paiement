<?php
/***************************************************************************
 *  Associaspip, extension de SPIP pour gestion d'associations             *
 *                                                                         *
 *  Copyright (c) 2007 Bernard Blazin & Francois de Montlivault (V1)       *
 *  Copyright (c) 2010-2011 Emmanuel Saint-James & Jeannot Lapin (V2)       *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'simplpaie'=>'Simplasso - paiement',
	'erreur password obligatoire' => 'La saisie d\'un mot de passe est obligatoire' ,
	'mot_de_passe' => 'Simplasso - paiement',
	'mot_de_passe_confirmation' => 'Simplasso - paiement',
	'label_renouvellement_cotisation' => 'Renouvellement de cotisation'

);







?>

<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_renouveler_cotisation_charger_dist()
{

    include_spip('inc/session');
    if (empty(session_get('id_auteur'))){
        echo('Erreur');
        exit();
    }
    $valeurs = [];
    $valeurs['_mes_saisies'] = simplasso_champs_renouveler_cotisation();
    return $valeurs;
}


function formulaires_renouveler_cotisation_verifier_dist()
{
    $mes_saisies = simplasso_champs_renouveler_cotisation();
    $erreurs = saisies_verifier($mes_saisies);
    return $erreurs;
}


function formulaires_renouveler_cotisation_traiter_dist()
{

    
    $tab_prestation = simplasso_liste_prestation();
    $tab_cotis = simplasso_tab_cotis();
    $id_cotis = (int)_request('cotisation') + 0;
    $montant_cotis = $tab_cotis[$id_cotis]['montant'];
    $montant_don=0;
    $tab_sr = [];
    if (_request('don_oui_non') === 'oui') {
        $montant_don = (int)_request('montant_don') + 0;
        $montant_don_autre = max(0, (integer)_request('montant_don_autre'));
        if ($montant_don === -1) {
            if ($montant_don_autre > 0) {
                $montant_don = $montant_don_autre;
            } else {
                $montant_don = 0;
            }
        }
        if ($montant_don > 0) {
            $id_prestation_don = key($tab_prestation['prestation']['4']);
            $tab_sr[] = ['nom' => 'Don', 'montant' => $montant_don, 'id_prestation' => $id_prestation_don];
        }
    }
    $tab_sr[] = ['nom' => 'Cotisation : ' . $tab_cotis[$id_cotis]['nom'], 'id_prestation' => $id_cotis, 'montant' => $montant_cotis];
    $contenu = ['type' => 'renouvelement', 'servicerendus' => $tab_sr];
    $options = ['champs' => ['contenu' => json_encode($contenu)]];
    $inserer_transaction = charger_fonction('inserer_transaction', 'bank');
    $id_transaction = $inserer_transaction($montant_don + $montant_cotis, $options);
	include_spip('inc/session');
    session_set('transaction', $id_transaction);
    $cible = generer_url_public('inscription_paiement');
    $res = [
        'message_ok' => 'Vos modifications ont bien été enregistrées.',
        'redirect' => $cible
    ];

    return $res;
}


function simplasso_champs_renouveler_cotisation()
{

    include_spip('inc/config');
    $tab_cotis = simplasso_tab_cotis();
    foreach ($tab_cotis as &$cotis) {
        $cotis = $cotis['montant'] . '€' . ' : ' . $cotis['nom'];
    }

    $tab_don = simplpaie_liste_don() + ['-1' => _T('simplasso:autre_montant')];

    $tab_champs = [

        'fieldset_cotisation' => [
            'saisie' => 'fieldset',
            'options' => [
                'nom' => 'cotisation',
                'label' => _T('simplpaie:label_renouvellement_cotisation'),
                'li_class' => 'fieldset_cotisation'
            ],
            'saisies' => [
                'cotisation' => [
                    'saisie' => 'radio',
                    'options' => [
                        'nom' => 'cotisation',
                        'label' => _T('simplasso:label_cotisation'),
                        'class' => 'casechoix',
                        'datas' => $tab_cotis,
                        'obligatoire' => 'oui'
                    ]
                ]
            ]
        ],


        'fieldset_don' => [
            'saisie' => 'fieldset',
            'options' => [
                'nom' => 'don',
            ],
            'saisies' => [
                // Champ Faire un don Oui/non
                'don_oui_non' => [
                    'saisie' => 'oui_non',
                    'options' => [
                        'nom' => 'don_oui_non',
                        'label' => _T('simplasso:don_question'),
                        'valeur_oui' => 'oui',
                        'valeur_non' => 'non'
                    ]
                ],
                'montant_don' => [
                    'saisie' => 'radio',
                    'options' => [
                        'nom' => 'montant_don',
                        'label' => _T('simplasso:montant_du_don'),
                        'class' => 'casechoix',
                        'datas' => $tab_don,

                    ]
                ],
                // Champ montant du renouveler_cotisation
                'montant_don_autre' => [
                    'saisie' => 'input',
                    'options' => [
                        'nom' => 'montant_don_autre',
                        'label' => _T('simplasso:label_montant_autre')
                    ]
                ]
            ]
        ]
    ];
    return $tab_champs;
}

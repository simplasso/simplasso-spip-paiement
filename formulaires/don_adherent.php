<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_don_adherent_charger_dist()
{
    include_spip('inc/session');
    if (empty(session_get('id_auteur'))){
        echo('Erreur');
        exit();
    }

    $valeurs = [];
    $valeurs['_mes_saisies'] = simplasso_champs_don_adherent();
    return $valeurs;
}


function formulaires_don_adherent_verifier_dist()
{

    $mes_saisies = simplasso_champs_don_adherent();
    $erreurs = saisies_verifier($mes_saisies);

    $montant_don = (int)_request('montant_don')+0;
    $montant_don_autre = ((integer)_request('montant_don_autre'))+0;
    if ($montant_don === -1 &&  $montant_don_autre <= 0){
        $erreurs['montant_don']='Erreur dans le montant du don';
    }



    return $erreurs;
}


function formulaires_don_adherent_traiter_dist()
{

    include_spip('inc/session');

    $tab_prestation = simplasso_liste_prestation();
    $montant_don = (int)_request('montant_don')+0;
    $montant_don_autre = (int)_request('montant_don_autre')+0;

    $tab_sr=[];
    $montant_don_ok = 0;
    if ($montant_don > 0 ){
        $montant_don_ok = $montant_don;
    }
    elseif ($montant_don == -1 &&  $montant_don_autre > 0){
        $montant_don_ok = $montant_don_autre;
    }

    if ($montant_don_ok > 0){
        $id_prestation_don = key($tab_prestation['prestation']['4']);
        $tab_sr[] =['nom'=>'Don','montant'=>$montant_don_ok,'id_prestation'=>$id_prestation_don];

        $contenu = ['type'=>'don','servicerendus'=>$tab_sr];
        $options = ['auteur_id'=>'1','id_auteur'=>session_get('id_auteur'),'champs'=>['contenu'=>json_encode($contenu)]];
        $inserer_transaction = charger_fonction('inserer_transaction', 'bank');
        $id_transaction = $inserer_transaction($montant_don_ok, $options);
        session_set('transaction', $id_transaction);

        $cible = generer_url_public('inscription_paiement');

    $res = [
        'message_ok' => 'Vos modifications ont bien été enregistrées.',
        'redirect' => $cible];
    }
    else{
        $res = [
            'message_erreur' => 'Veuillez saisir le montant de votre don.'];
    }
    return $res;
}


function simplasso_champs_don_adherent()
{
    $tab_don= simplpaie_liste_don()+['-1'=>_T('simplasso:label_autre_montant')];
    $tab_champs = [
        'fieldset_don' => [
            'saisie' => 'fieldset',
            'options' => [
                'nom' => 'don'
            ]
                ,
                'saisies' => [

                    'montant_don' => [
                        'saisie' => 'radio',
                        'options' => [
                            'nom' => 'montant_don',
                            'label' => _T('simplasso:montant_du_don'),
                            'class' => 'casechoix',
                            'datas' => $tab_don,

                        ]
                    ],
                    // Champ montant du don
                    'montant_don_autre' => [
                        'saisie' => 'input',
                        'options' => [
                            'nom' => 'montant_don_autre',
                            'label' => _T('simplasso:label_autre_montant')
                        ]]
                ]]
        ];


    return $tab_champs;
}

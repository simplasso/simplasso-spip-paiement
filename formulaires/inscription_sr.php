<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_inscription_sr_charger_dist()
{
    $valeurs = [];

    $valeurs['_mes_saisies'] = simplasso_inscription_sr();
    $tab_data = [];
    $info_adh = session_get('inscription_adh1');
    if (!isset($info_adh['email'])){
        include_spip('inc/headers');
        redirige_par_entete(generer_url_public('inscription_adherent'));
    }
    if (isset($info_adh['organisme'])) {
        $tab_data['cotisation'] = 3;
    } else {
        $tab_data['cotisation'] = 2;
    }

    $valeurs = array_merge($valeurs, $tab_data);
    return $valeurs;
}


function formulaires_inscription_sr_verifier_dist()
{
    $mes_saisies = simplasso_inscription_sr();
    $erreurs = saisies_verifier($mes_saisies);
    return $erreurs;
}


function formulaires_inscription_sr_traiter_dist()
{
    include_spip('inc/session');
    $tab_prestation = simplasso_liste_prestation();
    $tab_cotis = simplasso_tab_cotis();
    $id_cotis = (int)_request('cotisation') + 0;
    $montant_cotis = $tab_cotis[$id_cotis]['montant'];
    $data[] = ['nom' => 'Cotisation : ' . $tab_cotis[$id_cotis]['nom'], 'id_prestation' => $id_cotis, 'montant' => $montant_cotis];
    if (_request('don_oui_non') === 'oui') {
        $montant_don = (int)_request('montant_don') + 0;
        $montant_don_autre = (integer)_request('montant_don_autre');
        if ($montant_don === -1) {
            if ($montant_don_autre > 0) {
                $montant_don = $montant_don_autre;
            } else {
                $montant_don = 0;
            }
        }
        if ($montant_don > 0) {
            $id_prestation_don = key($tab_prestation['prestation']['4']);
            $data[] = ['nom' => 'Don', 'montant' => $montant_don, 'id_prestation' => $id_prestation_don];
        }
    }
    session_set('inscription_adh2', $data);
    $cible = generer_url_public('inscription_recap');
    $res = [
        'message_ok' => 'Vos modifications ont bien été enregistrées. Passage à l\'étape suivante',
        'redirect' => $cible
    ];

    return $res;
}


function simplasso_inscription_sr()
{

    include_spip('inc/config');
    $tab_cotis = simplasso_tab_cotis();
    foreach ($tab_cotis as &$cotis) {
        $cotis = $cotis['montant'] . '€' . ' : ' . $cotis['nom'];
    }
    $tab_don = simplpaie_liste_don() + ['-1' => _T('simplasso:autre_montant')];

    $tab_champs = [

        // Renouvellement Cotisation
        'fieldset_cotisation' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'cotisation',
                'label' => _T('simplasso:label_cotisation'),
                'li_class' => 'fieldset_cotisation'),
            'saisies' => array(

                // Champs choix du type de cotistaion
                'cotisation' => array(
                    'saisie' => 'radio',
                    'options' => array(
                        'nom' => 'cotisation',
                        'label' => _T('simplasso:label_type_cotisation'),
                        'class' => 'casechoix',
                        'datas' => $tab_cotis,
                        'obligatoire' => 'oui',

                    )
                ))),


        // DON
        'fieldset_don' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'don',
                'label' => _T('simplasso:label_don'),
                'li_class' => 'fieldset_don'),
            'saisies' => array(


                // Champ Faire un don Oui/non
                'don_oui_non' => array(
                    'saisie' => 'oui_non',
                    'options' => array(
                        'nom' => 'don_oui_non',
                        'label' => _T('simplasso:don_question'),
                        'valeur_oui' => 'oui',
                        'valeur_non' => 'non'

                    )
                ),

                // Champ montant du don
                'montant_don' => array(
                    'saisie' => 'radio',
                    'options' => array(
                        'nom' => 'montant_don',
                        'label' => _T('simplasso:label_montant_don'),
                        'class' => 'casechoix',
                        'datas' => $tab_don,

                    )
                ),

                // Champ autre montant don
                'montant_don_autre' => array(
                    'saisie' => 'input',
                    'options' => array(
                        'nom' => 'montant_don_autre',
                        'label' => _T('simplasso:label_montant_autre')
                    )),
            ))
    ];


    return $tab_champs;
}

<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
 * \***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_inscription_recap_charger_dist()
{

    $info_adh = session_get('inscription_adh1');
    $info_sr = session_get('inscription_adh2');
    if (!isset($info_adh['email']) && !isset($info_sr['nom'])){
        include_spip('inc/headers');
        redirige_par_entete(generer_url_public('inscription_adherent'));
    }

    $valeurs = [];
    $valeurs['_mes_saisies'] = simplasso_inscription_recap();

    $total = 0;
    $msg = [];
    foreach ($info_sr as $sr) {
        $total += $sr['montant'];
        $msg[] = $sr['nom'] . ' : ' . $sr['montant'];
    }

    $valeurs['_message'] = 'Identité : ' . $info_adh['nom_famille'] . ' ' . $info_adh['prenom'] . '<br>' . implode('<br/>', $msg) . '<br><strong>Montant total : ' . $total . ' € </strong>';
    return $valeurs;
}


function formulaires_inscription_recap_verifier_dist()
{


    $mes_saisies = simplasso_inscription_recap();
    $erreurs = saisies_verifier($mes_saisies);

    $condition = _request('accepter_condition');
    if ($condition !== 'oui') {
        $erreurs['accepter_condition'] = _T('simplasso:form_erreur_accepter_condition');
    }

    return $erreurs;
}


function formulaires_inscription_recap_traiter_dist()
{

    $info_adh = session_get('inscription_adh1');
    $info_sr = session_get('inscription_adh2');

    $total = 0;
    foreach ($info_sr as $sr) {

        $total += $sr['montant'];
    }

    $tab_rgpd = interrogeAPIinscription('info_rgpd');
    $tab_rgpd_reponse = [];
    foreach ($tab_rgpd as $rgpd) {
        $id = $rgpd['id_rgpd_question'];
        $tab_rgpd_reponse[$id] = _request('rgpd_question_' . $id);
    }

    $contenu = ['type' => 'inscription', 'servicerendus' => $info_sr, 'individu' => $info_adh, 'rgpd' => $tab_rgpd_reponse];
    $options = ['champs' => ['contenu' => json_encode($contenu)]];
    $inserer_transaction = charger_fonction('inserer_transaction', 'bank');
    $id_transaction = $inserer_transaction($total, $options);
    include_spip('inc/session');
    session_set('transaction', $id_transaction);
    $cible = generer_url_public('inscription_paiement');
    $res = [
        'message_ok' => 'Vos modifications ont bien été enregistrées.',
        'redirect' => $cible];
    return $res;
}


function simplasso_inscription_recap()
{
    include_spip('inc/config');
    $tab_rgpd = interrogeAPIinscription('info_rgpd');
    $tab_questions = [];
    foreach ($tab_rgpd as $rgpd) {
        $id = $rgpd['id_rgpd_question'];
        $tab_questions['rgpd_question_' . $id] =
            [
                'saisie' => 'oui_non',
                'options' => [
                    'nom' => 'rgpd_question_' . $id,
                    'label' => $rgpd['nom'],
                    'valeur_oui' => 'oui',
                    'valeur_non' => 'non'
                ]
            ];
    }


    $tab_champs = [
        'fieldset_rgpd' => [
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'identite',
                'label' => '',
                'li_class' => 'fieldset_personne'),
            'saisies' => $tab_questions
        ]
    ];


    $tab_champs ['fieldset_condition'] = [
        'saisie' => 'fieldset',
        'options' => [
            'nom' => 'identite',
            'label' => '',
            'li_class' => 'fieldset_personne'],
        'saisies' => [
            // contact_souhait
            [
                'saisie' => 'oui_non',
                'options' => [
                    'nom' => 'accepter_condition',
                    'label' => filtrer_entites(_T('simplasso:label_accepter_condition')),
                    'class' => '',
                    'valeur_oui' => 'oui',
                    'valeur_non' => 'non',
                ]
            ]
        ]

    ];


    return $tab_champs;
}

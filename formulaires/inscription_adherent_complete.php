<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_inscription_adherent_charger_dist()
{
    $valeurs = [];
    $valeurs['_mes_saisies'] = simplasso_inscription_adherent();
    $tab_data = [
        'pays' => 'FR'
    ];
    $valeurs = array_merge($valeurs, $tab_data);
    return $valeurs;
}


function formulaires_inscription_adherent_verifier_dist()
{

    $mes_saisies = simplasso_inscription_adherent();
    $erreurs = saisies_verifier($mes_saisies);
    $ok = true;
    /*  $email = _request('email');
      if (!empty($email)) {
          $ok = interrogeAPIactivation('adherent_test_email', array('email' => $email));
          var_dump($ok);
      }*/

    if (!$ok) {
        $erreurs['email'] = _T('form_email_non_valide');
    }
    return $erreurs;
}


function formulaires_inscription_adherent_traiter_dist()
{
    $tab_champs = simplasso_inscription_adherent();
    $args = [];
    foreach ($tab_champs as $fieldset) {

        foreach ($fieldset['saisies'] as $champs) {
            $ch = $champs['options']['nom'];
            $args[$ch] = trim(_request($ch));
        }
    }

    include_spip('inc/session');
    session_set('inscription_adh1', $args);

    $cible = generer_url_public('inscription_sr');
    $res = [
        'message_ok' => 'Vos modifications ont bien été enregistrées.',
        'redirect' => $cible];

    return $res;
}


function simplasso_inscription_adherent()
{

    $options = interrogeAPIinscription("options");

    $adresse_obligatoire = true;
    $courriel_obligatoire = 'oui';
    $saisie_identite = [];


    if ($options['champs_individu']['civilite']) {
        // Champ civilité
        if ($options['civilite']['libre']) {

            $saisie_identite['civlite'] = array(
                'saisie' => 'input',
                'options' => array(
                    'nom' => 'civilite',
                    'label' => _T('simplasso:label_civilite')
                ));


        } else {
            $data_civi = $options['civilite']['valeurs'];
            $saisie_identite['civlite'] = array(
                'saisie' => 'radio',
                'options' => array(
                    'nom' => 'civilite',
                    'label' => _T('simplasso:label_civilite'),
                    'datas' => $data_civi
                ));

        }

    }
    $saisie_identite['nom_famille'] =     // Champ nom
        [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'nom_famille',
                'label' => _T('simplasso:label_nom'),
                'class' => '',
                'obligatoire' => 'oui'
            )];


    $saisie_identite['prenom'] =    // Champ prénom
        [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'prenom',
                'label' => _T('simplasso:label_prenom'),
                'class' => '',
            )];


    $saisie_coordonnees = [
        //Champ adresse
        array(
            'saisie' => 'textarea',
            'options' => array(
                'nom' => 'adresse',
                'label' => _T('simplasso:label_adresse'),
                'class' => '',
                'rows' => 2,
                'obligatoire' => $adresse_obligatoire
            ),),

        //Champ code postal
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'code_postal',
                'label' => _T('simplasso:label_code_postal'),
                'class' => '',
                'obligatoire' => $adresse_obligatoire
            )),

        //Champ commune
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'ville',
                'label' => _T('simplasso:label_ville'),
                'class' => 'autocomplete',
                'obligatoire' => $adresse_obligatoire
            )),

        //Champ Pays
        'pays' => array(
            'saisie' => 'selection',
            'options' => array(
                'nom' => 'pays',
                'label' => _T('simplasso:pays'),
                'class' => '',
                'datas' => liste_pays(),
                'obligatoire' => 'oui',

            )
        )];


    $saisie_communication = [
        // Telephone fixe
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'telephone',
                'label' => filtrer_entites(_T('simplasso:label_telephone_fixe')),
                'class' => 'masque_telephone',)),

        // Telephone portable
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'mobile',
                'label' => filtrer_entites(_T('simplasso:label_telephone_mobile')),
                'class' => 'masque_telephone',))
    ];


    // telephone_pro
    if ($options['champs_individu']['telephone_pro']) {
        $saisie_communication['telephone_pro'] = [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'telephone_pro',
                'label' => filtrer_entites(_T('simplasso:label_telephone_pro')),
                'class' => 'masque_telephone',)];
    }


    // Fax
    if ($options['champs_individu']['fax']) {
        $saisie_communication['fax'] = [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'fax',
                'label' => filtrer_entites(_T('simplasso:label_fax')),
                'class' => 'masque_telephone',)];
    }
    // Email
    $saisie_communication['email'] = [
        'saisie' => 'input',
        'options' => array(
            'nom' => 'email',
            'label' => filtrer_entites(_T('simplasso:label_courriel')),
            'class' => '',
            'obligatoire' => $courriel_obligatoire
        )];


    $saisie_divers = [];


    if ($options['champs_individu']['profession']) {

        //Champ Profession
        $saisie_divers['profession'] = [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'profession',
                'label' => _T('simplasso:label_profession'),
            )];
    }


    //Champ Sexe
    if ($options['champs_individu']['sexe']) {
        $saisie_divers['sexe'] = [
            'saisie' => 'radio',
            'options' => array(
                'nom' => 'sexe',
                'label' => _T('simplasso:label_sexe'),
                'datas' => array('H' => 'Homme', 'F' => 'Femme', 'A' => 'Autre'),
            )];
    }
    //Champ date de naissance
    if ($options['champs_individu']['date_naissance']) {
        $saisie_divers['date_naissance'] = [
            'saisie' => 'date',
            'options' => array(
                'nom' => 'date_naissance',
                'label' => _T('simplasso:label_date_de_naissance')
            ),
            'verifier' => array(
                'type' => 'date',
                'options' => array('normaliser' => 'datetime')
            )];
    }


    //Champ annee de naissance
    if ($options['champs_individu']['annee_naissance']) {
        $saisie_divers['annee_naissance'] = [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'annee_naissance',
                'label' => _T('simplasso:label_annee_de_naissance')
            )];
    }


    include_spip('inc/config');
    $tab_prestation = simplasso_liste_prestation();
    $tab_cotis = [];
    foreach ($tab_prestation['details'][1] as $id => $prestation) {

        if ((int)$prestation['prestation_type'] === 1) {
            $tab_cotis[$id] = $prestation['nom'] . ' : ' . $prestation['prix'][0]['montant'] . '€';
        }

    }
    $tab_don = simplpaie_liste_don();

    $tab_champs = [


        'fieldset_cotisation' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'cotisation',
                'label' => _T('simplasso:label_cotisation'),
                'li_class' => 'fieldset_cotisation'),
            'saisies' => array(


                'cotisation' => array(
                    'saisie' => 'radio',
                    'options' => array(
                        'nom' => 'cotisation',
                        'label' => _T('simplasso:label_cotisation'),
                        'class' => '',
                        'datas' => $tab_cotis,
                        'obligatoire' => 'oui',

                    )
                ))),

        'fieldset_identite' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'don',
                'label' => _T('simplasso:label_don'),
                'li_class' => 'fieldset_don'),
            'saisies' => array(

                'montant_don' => array(
                    'saisie' => 'radio',
                    'options' => array(
                        'nom' => 'montant_don',
                        'label' => _T('simplasso:label_cotisation'),
                        'class' => 'casechoix',
                        'datas' => $tab_don,

                    )
                ),

                // Champ montant du don
                'montant_don_autre' => array(
                    'saisie' => 'input',
                    'options' => array(
                        'nom' => 'montant_don_autre',
                        'label' => _T('simplasso:label_montant_autre')
                    )),


            ))


    ];


    $tab_champs += [

        'fieldset_identite' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'identite',
                'label' => _T('simplasso:label_identite'),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_identite
        ),

        'fieldset_coordonnees' => [
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'coordonnees',
                'label' => filtrer_entites(_T('simplasso:label_coordonnees')),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_coordonnees
        ],

        'fieldset_communication' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'communication',
                'label' => _T('simplasso:label_moyen_de_communication'),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_communication),


        'fieldset_divers' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'divers',
                'label' => _T('simplasso:label_divers'),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_divers
        )
    ];


    $tab_champs += [

        'fieldset_condition' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'identite',
                'label' => '',
                'li_class' => 'fieldset_personne'),
            'saisies' => array(


                // contact_souhait
                array(
                    'saisie' => 'oui_non',
                    'options' => array(
                        'nom' => 'accepter_condition',
                        'label' => filtrer_entites(_T('simplasso:label_accepter_condition')),
                        'class' => '',
                        'valeur_oui' => 'oui',
                        'valeur_non' => 'non',
                    )),
            ))
    ];


    return $tab_champs;
}

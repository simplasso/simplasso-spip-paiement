<?php


if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('simplasso_fonctions');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_inscription_adherent_charger_dist()
{

    $valeurs = ['pays' => 'FR'];
    $valeurs['_info_cplt'] = '<span class="obligatoire">*</span> : ' . _T('simplasso:info_cplt_obligatoire');
    $options = interrogeAPIinscription("options");
    $valeurs['civilite_organisme'] = $options['civilite_organisme']??[];
    $valeurs['civilite'] = '';
    $valeurs['_mes_saisies'] = simplasso_inscription_adherent();
    if (!empty(_request('pays'))) {
        $valeurs = array_merge($valeurs, saisies_charger_champs(simplasso_inscription_adherent()));
    }
    return $valeurs;
}


function formulaires_inscription_adherent_verifier_dist()
{

    $mes_saisies = simplasso_inscription_adherent();
    $erreurs = saisies_verifier($mes_saisies);
    $email = _request('email');
    if (!empty($email)) {
        $reponse = interrogeAPIactivation('test', array('email' => $email));
        if ($reponse['ok']) {
            $erreurs['email'] = _T('simplasso:erreur_email_deja_enregistre', array('email' => htmlspecialchars($email)));
        }
    }

    $password = _request('mot_de_passe');
    $password2 = _request('mot_de_passe2');
    if (empty($password)) {
        $erreurs['mot_de_passe'] = _T('simplasso:erreur_password_obligatoire');
    } else if (strlen($password) < 8) {
        $erreurs['mot_de_passe'] = _T('simplasso:erreur_password_trop_court');
    } else if ($password !== $password2) {
        $erreurs['mot_de_passe'] = _T('simplasso:erreur_password_non_identique');
    }



    return $erreurs;
}


function formulaires_inscription_adherent_traiter_dist()
{
    $tab_champs = simplasso_inscription_adherent();
    $args = [];
    foreach ($tab_champs as $fieldset) {
        foreach ($fieldset['saisies'] as $champs) {
            $ch = $champs['options']['nom'];
            $args[$ch] = trim(_request($ch));
        }
    }
    $options = interrogeAPIinscription("options");
    if ($options['adherent']['nom_famille_majuscule'] == 1) {
        $args['nom_famille'] = strtoupper($args['nom_famille']);
    } elseif ($options['adherent']['nom_famille_majuscule'] == 2) {
        $args['nom_famille'] = ucfirst($args['nom_famille']);
    }
    if ($options['adherent']['prenom_majuscule'] == 1) {
        $args['prenom'] = strtoupper($args['prenom']);
    } elseif ($options['adherent']['prenom_majuscule'] == 2) {
        $args['prenom'] = ucfirst($args['prenom']);
    }

    $tab_civilito = explode('|',$options['civilite_organisme']);
    if (!in_array($args['civilite'],$tab_civilito)){
        unset($args['organisme']);
    }

    include_spip('inc/session');
    session_set('inscription_adh1', $args);
    $cible = generer_url_public('inscription_sr');
    $res = [
        'message_ok' => 'Vos modifications ont bien été enregistrées. Passage à l\'étape suivante',
        'redirect' => $cible];
    return $res;
}


function simplasso_inscription_adherent()
{

    $options = interrogeAPIinscription("options");
    $adresse_obligatoire = $options['adresse_obligatoire'] ? 'oui' : '';
    $courriel_obligatoire = $options['courriel_obligatoire'] ? 'oui' : '';
    $tab_class_majuscule = ['', 'majuscule', 'capitalize'];
    $class_majuscule_nom = $tab_class_majuscule[$options['adherent']['nom_famille_majuscule']];
    $class_majuscule_prenom = $tab_class_majuscule[$options['adherent']['prenom_majuscule']];

    $info_obligatoire = '*';
    $saisie_identite = [];

    if ($options['champs_individu']['civilite']) {
        // Champ civilité
        if ($options['civilite']['libre']) {
            $saisie_identite['civlite'] = array(
                'saisie' => 'input',
                'options' => array(
                    'nom' => 'civilite',
                    'label' => _T('simplasso:label_civilite')
                ));
        } else {
            $data_civi = $options['civilite']['valeurs'];
            $saisie_identite['civlite'] = array(
                'saisie' => 'radio',
                'options' => array(
                    'nom' => 'civilite',
                    'label' => _T('simplasso:label_civilite'),
                    'datas' => $data_civi
                ));
        }

    }

    $saisie_identite['organisme'] =    // Champ organisme
        [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'organisme',
                'label' => _T('simplasso:label_organisme'),
                'class' => 'semi_obligatoire'
            )];


    $saisie_identite['nom_famille'] =     // Champ nom
        [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'nom_famille',
                'label' => _T('simplasso:label_nom'),
                'class' => $class_majuscule_nom,
                'obligatoire' => 'oui',
                'info_obligatoire' => $info_obligatoire

            )];


    $saisie_identite['prenom'] =    // Champ prénom
        [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'prenom',
                'label' => _T('simplasso:label_prenom'),
                'class' => $class_majuscule_prenom,
            )];




    $saisie_coordonnees = [
        //Champ adresse
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'adresse',
                'label' => _T('simplasso:label_adresse'),
                'class' => '',
                'obligatoire' => $adresse_obligatoire,
                'info_obligatoire' => $info_obligatoire
            )
        ),

        //Champ adresse
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'adresse_cplt',
                'label' => _T('simplasso:label_adresse_cplt'),
                'class' => ''

            )
        ),

        //Champ code postal
        array(
            'saisie' => 'code_postal',
            'options' => array(
                'nom' => 'code_postal',
                'label' => _T('simplasso:label_code_postal'),
                'class' => '',
                'attributs'=> 'data-ville="champ_ville"',
                'obligatoire' => $adresse_obligatoire,
                'info_obligatoire' => $info_obligatoire
            )),

        //Champ commune
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'ville',
                'label' => _T('simplasso:label_ville'),
                'class' => 'autocomplete',
                'obligatoire' => $adresse_obligatoire,
                'info_obligatoire' => $info_obligatoire
            )),

        //Champ Pays
        'pays' => array(
            'saisie' => 'selection',
            'options' => array(
                'nom' => 'pays',
                'label' => _T('simplasso:pays'),
                'class' => '',
                'datas' => liste_pays(),
                'obligatoire' => 'oui',
                'info_obligatoire' => $info_obligatoire

            )
        )];


    $saisie_communication = [
        // Telephone fixe
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'telephone',
                'label' => filtrer_entites(_T('simplasso:label_telephone_fixe')),
                'class' => 'masque_telephone',)),

        // Telephone portable
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'mobile',
                'label' => filtrer_entites(_T('simplasso:label_telephone_mobile')),
                'class' => 'masque_telephone',))
    ];


    // telephone_pro
    if ($options['champs_individu']['telephone_pro']) {
        $saisie_communication['telephone_pro'] = [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'telephone_pro',
                'label' => filtrer_entites(_T('simplasso:label_telephone_pro')),
                'class' => 'masque_telephone',)];
    }


    // Fax
    if ($options['champs_individu']['fax']) {
        $saisie_communication['fax'] = [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'fax',
                'label' => filtrer_entites(_T('simplasso:label_fax')),
                'class' => 'masque_telephone',)];
    }
    // Email
    $saisie_communication['email'] = [
        'saisie' => 'email',
        'options' => array(
            'nom' => 'email',
            'label' => filtrer_entites(_T('simplasso:label_courriel')),
            'class' => '',
            'obligatoire' => $courriel_obligatoire,
            'info_obligatoire' => $info_obligatoire
        )];


    $saisie_divers = [];


    if ($options['champs_individu']['profession']) {

        //Champ Profession
        $saisie_divers['profession'] = [
            'saisie' => 'input',
            'options' => array(
                'nom' => 'profession',
                'label' => _T('simplasso:label_profession'),
            )];
    }


    //Champ Sexe
    if ($options['champs_individu']['sexe']) {
        $saisie_divers['sexe'] = [
            'saisie' => 'radio',
            'options' => array(
                'nom' => 'sexe',
                'label' => _T('simplasso:label_sexe'),
                'datas' => array('H' => 'Homme', 'F' => 'Femme', 'A' => 'Autre')
            )];
    }


    //Champ  naissance
    if ($options['champs_individu']['naissance']) {


        //Champ annee de naissance
        if ($options['adherent']['annee_naissance']) {
            $date_du_jour = new DateTime();
            $saisie_divers['annee_naissance'] = [
                'saisie' => 'input',
                'options' => array(
                    'type' => 'number',
                    'attributs' => 'min="1900" max="' . $date_du_jour->format('Y') . '"',
                    'nom' => 'annee_naissance',
                    'label' => _T('simplasso:label_annee_de_naissance')
                )];
        } else {
            //Champ date de naissance
            $saisie_divers['date_naissance'] = [
                'saisie' => 'date',
                'options' => array(
                    'nom' => 'date_naissance',
                    'label' => _T('simplasso:label_date_de_naissance')
                ),
                'verifier' => array(
                    'type' => 'date',
                    'options' => array('normaliser' => 'datetime')
                )];

        }
    }


    //Champ Mot de passe
    $saisie_divers['mot_de_passe'] = [
        'saisie' => 'input',
        'options' => array(
            'nom' => 'mot_de_passe',
            'type' => 'password',
            'label' => _T('simplasso:label_mot_de_passe'),
            'obligatoire' => $courriel_obligatoire,
            'info_obligatoire' => $info_obligatoire
        ),
    ];


    $saisie_divers['mot_de_passe2'] = [
        'saisie' => 'input',
        'options' => array(
            'nom' => 'mot_de_passe2',
            'type' => 'password',
            'label' => _T('simplasso:label_mot_de_passe_confirmation'),
            'obligatoire' => $courriel_obligatoire,
            'info_obligatoire' => $info_obligatoire
        ),
    ];


    $saisie_communication['email'] = [
        'saisie' => 'email',
        'options' => array(
            'nom' => 'email',
            'label' => filtrer_entites(_T('simplasso:label_courriel')),
            'class' => '',
            'obligatoire' => $courriel_obligatoire,
            'info_obligatoire' => $info_obligatoire
        )];


    $tab_champs = array(

        'fieldset_identite' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'identite',
                'label' => _T('simplasso:label_identite'),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_identite
        ),


        'fieldset_coordonnees' => [
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'coordonnees',
                'label' => filtrer_entites(_T('simplasso:label_coordonnees')),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_coordonnees
        ],


        'fieldset_communication' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'communication',
                'label' => _T('simplasso:label_moyen_de_communication'),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_communication),


        'fieldset_divers' => array(
            'saisie' => 'fieldset',
            'options' => array(
                'nom' => 'divers',
                'label' => _T('simplasso:label_divers'),
                'li_class' => 'fieldset_personne'),
            'saisies' => $saisie_divers
        ));


    return $tab_champs;
}

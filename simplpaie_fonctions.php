<?php


if (!defined("_ECRIRE_INC_VERSION")) return;



function simplpaie_liste_don() {

    return [
        '200'=>'200 €',
        '100'=>'100 €',
        '50'=>'50 €',
        '30'=>'30 €',
        '20'=>'20 €',
        '10'=>'10 €'
    ];

}



function simplasso_tab_cotis(){

    $tab_prestation = simplasso_liste_prestation();
    $tab_cotis = [];
    $time =time();
    foreach ($tab_prestation['details'][1] as $id => $prestation) {
        if ((int)$prestation['prestation_type'] === 1) {
            foreach ($prestation['prix'] as $indice=>$prix) {
                $tarif = $prix['montant'];
                if ($time<$prix['date_fin']){
                    break;
                }
            }
            $tab_cotis[$id] = ['nom'=>$prestation['nom'],'montant'=>$tarif];
        }
    }
    return $tab_cotis;
}



